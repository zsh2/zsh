# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' prompt 'Corrections: %e error(s)'
zstyle ':compinstall'  filename "$HOME/.zshrc"

autoload -Uz compinit
compinit

# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory extendedglob nomatch notify sharehistory
bindkey -e

# End of lines configured by zsh-newuser-install

# Files split into smaller files and sourced in order
# eg .zsh.d/S01_something 
for zshrc_snipplet in ~/.zsh.d/S[0-9][0-9]*[^~] ; do
    source $zshrc_snipplet
done


